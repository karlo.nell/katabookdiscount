﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BookStoreDiscount
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            // buy 2 different books - 5% discount
            // buy 3 different books - 10% discount
            // buy 4 different books - 20% discount
            Console.WriteLine(BookDiscountCalculator(new int[] {1, 1, 1 }));
            Console.ReadLine();
        }
        public static double BookDiscountCalculator(int[] books)
        {
            List<int> booksList = books.ToList();

            //Check if any books
            if (booksList.Count() == 0) return 0;

            //Declare unique books count
            int uniqueBooks = booksList.Distinct().Count();

            //Declare pairs
            int bookPairs = booksList.GroupBy(b => b).Where(book => book.Count() > 1).Count();

            //IDK why 1 works but not 0 
            if ((bookPairs == 1))
            {
                // (UniqueBooks * 8 EURO * Discount) + (8 EURO * (TotalBooks - UniqueBooks))
                return Convert.ToDouble(uniqueBooks) * 8.0 * GetDiscountInterval(uniqueBooks) + (8 * (booksList.Count() - uniqueBooks));
            }
            else
            {
                // (8 EURO * bookPairs * getdiscountinterval(bookPairs)) + (8 EURO * uniqueBooks * GetDiscountInterval(uniqueBooks))
                return (8 *Convert.ToDouble(bookPairs) * GetDiscountInterval(bookPairs)) + (8 * (Convert.ToDouble(uniqueBooks) * GetDiscountInterval(uniqueBooks)));
            }
        }
        public static double GetDiscountInterval(int uniqueBooks)
        {
            switch (uniqueBooks)
            {
                case 1:
                    return 1;
                case 2:
                    return 0.95;
                case 3:
                    return 0.90;
                case 4:
                    return 0.80;
                default:
                    return 1;
            }
        }
    }

}


